
// Whatever to be done in the main "setup" loop
void u8g2_setup(void) {
	u8g2.begin();
	pinMode(oledSleepButtonPin, INPUT_PULLUP);
	u8g2.setPowerSave(0);
}


void u8g2_prepare(void) {
	u8g2.setFont(u8g2_font_6x10_tf);
	u8g2.setFontRefHeightExtendedText();
	u8g2.setDrawColor(1);
	u8g2.setFontPosTop();
	u8g2.setFontDirection(0);
}

void u8g2_loop() {
	int timeToSd = loopUpdatePeriod[sdcard] - timerLoopPeriod[sdcard];
	if ((timeToSd > timerLoopDuration[oled]) && ShouldLoopStart(oled)) // This OLED loop is the longest (around 35ms for now. Give preference lo SD if needed
	{
		// check if button to switch it off is pressed. If so, simply go to low power consumption
		// This is done inside here to benefit the flying condition (when OLED will be usually OFF)
		if (digitalRead(oledSleepButtonPin) == HIGH) {
			u8g2.setPowerSave(1);
			oledIsOff = true;
			LoopDone(oled);
			//Serial.println("Oled High");
			return;
		}
		else {
			u8g2.setPowerSave(0);
			oledIsOff = false;
			//Serial.println("Oled Low");
		}

		u8g2.clearBuffer();
		u8g2_prepare();
		u8g2_stats();
		u8g2_level(attitude.pitch, attitude.yaw, attitude.roll, globalPosition.relative_alt);
		u8g2_vor(vor, int(globalPosition.hdg) / 100);
		u8g2_glow(glowMode, gasThreshold, gasPos, glowState);
		u8g2_gps(glowMode, gasThreshold, gasPwmRawValue, glowState);
		u8g2_irrpm(rpm);

		// Send everything to the display
		u8g2.sendBuffer();

		// Update last loop duration
		/*u8g2_duration =  millis() - u8g2_duration;
		Serial.print("u8g2 loop time: ");
		Serial.println(u8g2_duration);*/

		// Ack finished
		LoopDone(oled);
	}
}

void u8g2_printlnNow(String& msg) {
	if (currentCursorRow > (64 - FONT_HEIGHT)) {
		u8g2.clearBuffer();
		currentCursorRow = 0;
	}	
	u8g2.setCursor(0, currentCursorRow);
	u8g2.print(msg);
	currentCursorRow = currentCursorRow + FONT_HEIGHT;
	u8g2.sendBuffer();
}

void u8g2_level(float pitch, float yaw, float roll, float altitude) {

	u8g2.setCursor(0, 0);
	//u8g2.print("y");
	u8g2.print(yaw * 360 / (2 * PI), 0);
	u8g2.print("/");
	u8g2.print(pitch * 360 / (2 * PI), 0);
	u8g2.print("/");
	u8g2.print(roll * 360 / (2 * PI), 0);

	u8g2.drawFrame(HOR_X_CENTER - HOR_BOX_W / 2, HOR_Y_CENTER - HOR_BOX_H / 2, HOR_BOX_W, HOR_BOX_H);
	u8g2.drawCircle(HOR_X_CENTER, HOR_Y_CENTER, HOR_RAD);

	int yawX = int(cos(yaw)*HOR_RAD);
	int yawY = int(sin(yaw)*HOR_RAD);
	u8g2.drawDisc(HOR_X_CENTER + yawX, HOR_Y_CENTER + yawY, HOR_TICK_SIZE);

	int rollX = int(cos(roll)*HOR_RAD);
	int rollY = int(sin(roll)*HOR_RAD);
	int pitchX = int(cos(pitch)*HOR_RAD);
	int pitchY = int(sin(pitch)*HOR_RAD);

	u8g2.drawLine(HOR_X_CENTER - rollX, HOR_Y_CENTER + rollY + pitchY, HOR_X_CENTER + rollX, HOR_Y_CENTER - rollY + pitchY); // horizon line (move with pitch and roll)
	u8g2.drawLine(HOR_X_CENTER - HOR_BOX_W / 2, HOR_Y_CENTER, HOR_X_CENTER + HOR_BOX_W / 2 - 1, HOR_Y_CENTER); // horizontal line (static)
}


void u8g2_stats() {
	u8g2.setCursor(ARM_X_POS, ARM_Y_POS);
	if (armed == 1) {
		u8g2.print("ARMED");
		u8g2.drawRFrame(ARM_X_POS, ARM_Y_POS, ARM_BOX_W, ARM_BOX_H, 3);
	}
	else {
		u8g2.print("SAFE");
	}

	u8g2.setCursor(ARM_X_POS, ARM_Y_POS + (FONT_HEIGHT + 1));
	u8g2.print(sysStatus.load/10); u8g2.print("% ");
	u8g2.print(voltage[1],1); u8g2.print("V");
	u8g2.setCursor(ARM_X_POS, ARM_Y_POS + 2*(FONT_HEIGHT + 1));
	u8g2.print(voltage[2],1); u8g2.print("V ");
	u8g2.print(voltage[3],1); u8g2.print("V");
	u8g2.setCursor(ARM_X_POS, ARM_Y_POS + 3 * (FONT_HEIGHT + 1));
	u8g2.print("LOG: "); 
	if (digitalRead(audioButton) == LOW) u8g2.print("Y "); else u8g2.print("N ");
	if (reportToSerial)	u8g2.print("S");
	if (writeToFile)	u8g2.print("F");
}

void u8g2_irrpm(int myRpm) {

	//rpm = 3500; // Temporary while the rpm sensor is offline
	rpm = map(gasPos, gasPwmMin, gasPwmMax,RPM_LOWER_LIMIT,RPM_UPPER_LIMIT) ; // Use gas pos.  Temporary while the rpm sensor is offline

	if (showBottomRpm) {
		//float rpmPerCent = rpm * 100.0L / RPM_UPPER_LIMIT;
		u8g2.drawHLine(0, 63, map(rpm, RPM_LOWER_LIMIT, RPM_UPPER_LIMIT, 0, 128));
		u8g2.setDrawColor(2);
		u8g2.drawPixel(map(rpmThreshold, RPM_LOWER_LIMIT, RPM_UPPER_LIMIT, 0, 128), 63);
		u8g2.setDrawColor(1);
	}

	u8g2.setCursor(RPM_X_POS, RPM_Y_POS);
	u8g2.print(myRpm);
	u8g2.print("rpm");
}


void u8g2_vor(int vor, int heading) {
	u8g2.drawFrame(VOR_X_CENTER - VOR_BOX_W / 2, VOR_Y_CENTER - VOR_BOX_H / 2, VOR_BOX_W, VOR_BOX_H);
	u8g2.drawCircle(VOR_X_CENTER, VOR_Y_CENTER, VOR_RAD);

	u8g2.drawLine(VOR_X_CENTER, VOR_Y_CENTER - VOR_BOX_H / 2 + 1, VOR_X_CENTER + vor, VOR_Y_CENTER + VOR_BOX_H / 2); // VOR line


	int vorX = int(sin(heading * 2.0 * PI / 360.0)*VOR_RAD);
	int vorY = int(cos(heading * 2.0 * PI / 360.0)*VOR_RAD);
	u8g2.drawDisc(VOR_X_CENTER + vorX, VOR_Y_CENTER + vorY, VOR_TICK_SIZE);

	u8g2.setCursor(VOR_X_CENTER - VOR_BOX_W / 2 + 2, VOR_Y_CENTER + VOR_BOX_H / 2 + 2);
	u8g2.print(heading);

}



void u8g2_glow(int glowMode, int gasThreshold, int currentGas, int hotGlow) {

	//currentGas = 1800;

	if (showBottomGas) {
		u8g2.drawHLine(0, 62, map(currentGas, gasPwmMin, gasPwmMax, 0, 128));
		u8g2.setDrawColor(2);
		u8g2.drawPixel(map(gasThreshold, gasPwmMin, gasPwmMax, 0, 128), 62);
		u8g2.setDrawColor(1);
	}

	if (currentGas < gasPwmMin) {
		u8g2.setCursor(GLO_X_START, GLO_Y_START - 1);
		u8g2.print("LOCKED");
	}
	else {
		if (currentGas > gasPwmMax) {
			currentGas = gasPwmMax; // to prevent drawing stupid things...
		}
		u8g2.drawFrame(GLO_X_START, GLO_Y_START + 1, GLO_BOX_W, GLO_BOX_H - 2);
		u8g2.drawBox(GLO_X_START, GLO_Y_START + 1, GLO_BOX_W * (currentGas - gasPwmMin) / (gasPwmMax - gasPwmMin), GLO_BOX_H - 2);
		u8g2.drawVLine(GLO_X_START + GLO_BOX_W * (gasThreshold - gasPwmMin) / (gasPwmMax - gasPwmMin), GLO_Y_START, GLO_BOX_H);
		u8g2.setCursor(GLO_X_START + GLO_BOX_W + 1, GLO_Y_START - 1);

		u8g2.print("");
		u8g2.print(glowMode);
		if (gasServoIsInverted) { u8g2.print("i"); }


		if (hotGlow == 1) {
			u8g2.print("o");
			//u8g2.setFont(u8g2_font_unifont_t_symbols);
			//u8g2.drawGlyph(GLO_X_START + GLO_BOX_W + 5, GLO_Y_START - 1, 9749); // 9728 glow, 9749 coffee
			//u8g2.setFont(u8g2_font_6x10_tf);
		}
		else {
			u8g2.print(" ");
		}
	}


}


void u8g2_gps(int glowMode, int gasThreshold, int currentGas, int hotGlow) {
	//u8g2.drawFrame(GPS_X_START, GPS_Y_START + 1, GPS_BOX_W, GPS_BOX_H - 2);
	//u8g2.drawBox(GPS_X_START, GPS_Y_START + 1, GPS_BOX_W * (pwm_value - 900) / 1200, GPS_BOX_H - 2);
	//u8g2.drawVLine(GPS_X_START + GPS_BOX_W * (gasThreshold - 900) / 1200, GPS_Y_START, GPS_BOX_H);
	u8g2.setCursor(GPS_X_START, GPS_Y_START);

	u8g2.print("s");
	u8g2.print(gpsRaw.satellites_visible);
	//u8g2.print(")");
	u8g2.print("-");
	u8g2.print(gpsRaw.fix_type); //< 0-1: no fix, 2: 2D fix, 3: 3D fix. Some applications will not use the value of this field unless it is at least two, so always correctly fill in the fix.

}