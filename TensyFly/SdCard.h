// SdCard.h


#include <SD.h>
//#include <SPI.h>

const int chipSelect = BUILTIN_SDCARD;

bool reportToSerial = false;
bool writeToFile = true;

String nextMsg = ""; // general use

elapsedMillis sdLastPassTime = 0;
int SD_CYCLE_TIME = 250;

String sdConfigResultString = "";
byte sdColumnSeparator = 9;

char dataFileName[13] = "TeensyLg.txt";

//char* msg[] = { "Card failed, or not present",
//"Card initialized.",
//"Error opening tnsyfly.cfg",
//"Config loaded!",
//"Error opening datalog.txt" ,
//"Initializing SD card..." };


String iniSectionNames[] = { "Voltage" , "Voltage" , "Voltage" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "Glow" , "IrRpm" , "IrRpm" , "IrRpm" , "IrRpm" , "IrRpm" , "IrRpm" , "IrRpm" };
String iniVariableNames[] = { "voltNumOfSensors" , "voltUpdatePeriod" , "voltARef" , "glowCycleTime" , "gasPwmPin" , "glowPin" , "glowButton" , "debounce" , "shortWait" , "longWait" , "GAS_MIN_SAFETY" , "glowUseRPM" , "gasThreshold" , "rpmThreshold" , "gasPwmMin" , "gasPwmMax" , "gasServoIsInverted" , "RPM_CHECK_INTERVAL" , "irrpmFlashesPerRev " , "STATUS_LED_PIN" , "IR_LED_PIN" , "RPM_SENSOR_PIN" , "RPM_UPPER_LIMIT" , "RPM_LOWER_LIMIT" };
int iniVariableValues[] = { 3 , 2500 , 5 , 500 , 38 , 27 , 28 , 200 , 4000 , 10000 , 0 , 1 , 1000 , 2500 , 900 , 1700 , 1 , 500 , 4 , 13 , 26 , 25 , 10000 , 0 };
//int *iniVariables[] = { 0, 0, 0, 0, 0 };
int *ptrIniVariables[] = { &voltNumOfSensors, &voltUpdatePeriod, &voltARef, &glowCycleTime, &gasPwmPin, &glowPin, &glowButton, &debounce, &shortWait, &longWait, &GAS_MIN_SAFETY, &glowUseRPM, &gasThreshold, &rpmThreshold, &gasPwmMin, &gasPwmMax, &gasServoIsInverted, &RPM_CHECK_INTERVAL, &irrpmFlashesPerRev , &STATUS_LED_PIN, &IR_LED_PIN, &RPM_SENSOR_PIN, &RPM_UPPER_LIMIT, &RPM_LOWER_LIMIT };

int iniTotalVarNum = 24;
