// Timers.h


// loops that may make use of these timers (mavlink and uavtalk will not be throttled dow, they will run on each loop() loop and the condition is if there's something in Serial1 to be read.
enum loops {
	oled, glow, irrpm, mavlink, uavtalk, audiorpm, sdcard, voltageLoop, mpxtelem
};

String loopNames[] = {			"oled",	"glow",	"irrpm",	"mavlink",	"uavtalk",	"audiorpm",	"sdcard",	"voltageLoop" , "mpxtelem"};
bool printLoopTimes[] = {		true,	true,	false,		false,		false,		false,		false,		false ,		true };
uint16_t loopUpdatePeriod[] = { 100,	500,	500,		1,			1,			50,			100,		5000 ,		1000};

elapsedMillis timerLoopPeriod[9];
uint16_t timerActualLoopPeriod[9];
uint16_t timerLoopDuration[9];

int thisLoopRecMessages = 0;
bool mavlinkMsgsBars = false;