// 
// 
// 

//#include "MPXTelemetry.h"


void MPXTelemetry_Setup() {
	// For now, let's use Serial2, which makes use of pins 9 and 10
	Serial2.begin(mpxBauds); // to Arduino Micro to send Telemetry data
}

int mpxUpdateVar(int varNum) {
	switch (varNum)
	{ // now, do your stuff for each variable... yes, this is hardcoded and it MUST return an integer
		// If the target variable is an integer already, just return it. SOme others need massage
	case 0:
		return int(vfrHud.airspeed * 100); // Airspeed in m/s * 100
	case 1:
		 return int(vfrHud.groundspeed * 100); // Ground speed in m/s * 100
	case 2:
		return int(vfrHud.alt * 100); // Altitude in m * 100
	case 3:
		return vfrHud.heading;
	case 4:
		return int(vfrHud.climb * 100); // Climb rate in m/s * 100
		// there is a maximum of 32 cases (0..31)
	default:
		return 0;
		break;
	}
}

void MPXTelemetry_Loop() {
	if (ShouldLoopStart(mpxtelem)) {

		for (int i = 0; i <= 15; i++) {
			if (mpxVarToDisplay[i] != 100) {
				mpxAllVariablesList[mpxVarToDisplay[i]] = mpxUpdateVar(mpxVarToDisplay[i]);
			}
		}

		LoopDone(mpxtelem);
	}
}