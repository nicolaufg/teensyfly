// 
// 
// 



void GlowDriver_setup(void) {
	//////////////// Glow stuff
	// when pin gasPwmPin goes high, call the rising function to check the pulse length
	pinMode(gasPwmPin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(gasPwmPin), rising, RISING);

	pinMode(glowPin, OUTPUT);
	pinMode(glowButton, INPUT_PULLUP);
	/////////////////////// End Glow Stuff


}



void GlowDriver_loop() {

	// Bring the reading from the servo at this moment. Watch out if the servo is reversed 
	if (gasServoIsInverted) {
		gasPos = (gasPwmMax - gasPwmRawValue) + gasPwmMin;
	}
	else {
		gasPos = gasPwmRawValue;
	}

		// Check for button press
	if (digitalRead(glowButton) == LOW) { // it is an INPUT_PULLUP pin...
		GlowDrive_ManageButton();
	}

	// Light it up if necessary
	if (ShouldLoopStart(glow)) {
		glow_elapsed = 0;

		// check whether it is RPM based or Gas based
		int Pos;
		int Threshold;
		if (glowUseRPM) {
			Pos = rpm;
			Threshold = rpmThreshold; 
		} else if (glowUseGas) {
			Pos = gasPos;
			Threshold = gasThreshold;
		}		

		if (!glowIsEnabled || gasPos < gasPwmMin - GAS_MIN_SAFETY) { //  based only on Gas (locked)
			glowState = LOW;
		}
		else
		{
			if (!glowHasThreshold) {
				glowState = HIGH;
			}
			else {
				if (Pos <= Threshold) { // this may be GAS or RPM
					if (glowIsPulsed) { glowState = (glowState ^ glowIsPulsed); }
					else { glowState = HIGH; }
				}
				else { glowState = LOW; }
			}
		}
		// digitalWrite(glowPin, glowState);
		//Serial.print("Glow: ");
		//Serial.println(glowState);
		//Serial.print("PWM: ");
		//Serial.println(pwm_value);

		// Ack finished
		LoopDone(glow);
	}
}

void rising() {
	attachInterrupt(digitalPinToInterrupt(gasPwmPin), falling, FALLING);
	gasPwmRiseTime = micros();
}

void falling() {
	attachInterrupt(digitalPinToInterrupt(gasPwmPin), rising, RISING);
	gasPwmRawValue = micros() - gasPwmRiseTime;

}

void GlowDrive_ManageButton() {
	glow_button_down = 0;
	
	while (digitalRead(glowButton) == LOW) {
	} // do nothing... just count later on how long the button was down

	if (glow_button_down > longWait) {
		//Serial.print("** Calibrating gas... ");
		myWait = 0;
		uint16_t a = gasPwmRawValue;
		gasPwmMin = a;
		gasPwmMax = a;
		gasServoIsInverted = false;
		// Set upper and lower bounds. Do it wisely in case the channel is inverted!!
		while (myWait < longWait) { // Waitng for low gas position and then button press
			a = gasPwmRawValue;
			//Serial.print("** Gas: ");
			//Serial.print(a);
			//Serial.print("  Min: ");
			//Serial.print(gasPwmMin);
			//Serial.print("  Max: ");
			//Serial.print(gasPwmMax);
			//Serial.print("  Inv: ");
			//Serial.println(int(inverseGas));

			if (a < gasPwmMin) gasPwmMin = a;
			if (a > gasPwmMax) gasPwmMax = a;
			glow_button_down = 0;
			while (digitalRead(glowButton) == LOW) {}
			if (glow_button_down > debounce) gasServoIsInverted = true;
		}
		//gasMin = (pwm_value < gasMin) ? pwm_value : gasMin; //(number < 0) ? 1 : 0


	}
	else if (glow_button_down > shortWait) {
		gasThreshold = gasPos;
		rpmThreshold = rpm;
		// Serial.print("** Gas Threshold set to: ");
		// Serial.println(pwm_value);
	}
	else if (glow_button_down > debounce)
	{
		glowMode = (glowMode + 1) % 4;
		// Serial.print("Mode: ");
		// Serial.println(glowMode);

		switch (glowMode) {
		case 0:
			glowIsPulsed = false;
			glowIsEnabled = false;
			glowHasThreshold = false;
			// Serial.println("Mode 0 - OFF");
			break;
		case 1:
			glowIsPulsed = false;
			glowIsEnabled = true;
			glowHasThreshold = false;
			// Serial.println("Mode 1 - ON");
			break;
		case 2:
			glowIsPulsed = false;
			glowIsEnabled = true;
			glowHasThreshold = true;
			// Serial.println("Mode 2 - ON Below XXX");
			break;
		case 3:
			glowIsPulsed = true;
			glowIsEnabled = true;
			glowHasThreshold = true;
			// Serial.println("Mode 3 - Pulsed Below XXX");
			break;
		}

	}
}