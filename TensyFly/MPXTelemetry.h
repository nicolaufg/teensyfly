// MPXTelemetry.h
//
//#ifndef _MPXTELEMETRY_h
//#define _MPXTELEMETRY_h
//
//#if defined(ARDUINO) && ARDUINO >= 100
//	#include "arduino.h"
//#else
//	#include "WProgram.h"
//#endif
//
//
//#endif
//

/* This guy will send data periocally to an Arduino Micro (is it a micro???) which is able
to talk to the MULTIPLEX Telemtry bus. The Teensy can't do that (well, I can't do the Teensy
do that) because there are some kinky communications timings that the guy who developed the
library I'm planning to use provided for 8MHz and 16MHz controllers... and I'm not throttling 
down the Teensy for this purpose! Besides, I may use the code in the Arduino in other planes
without the Teensy, so it is just fine like this.

What is done here is a protocol to send data to the Micro in a way that the Micro knows what the data is
and then puts it in the right MULTIPLEX Telemetry channel and do other data specific tricks.
In fact that protocol was developed some time ago when I had to send some data from a RasPy using Python
to my MULTIPLEX System, so I'm just reusing the Micro side.

*/

int mpxBauds = 57600;
int mpxEnabled = 1;

int mpxAllVariablesList[32] = {};
int mpxChannelEnabled[] =	{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int mpxVarToDisplay[] =		{ 2, 0, 1, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //per each channel of the 16 available

