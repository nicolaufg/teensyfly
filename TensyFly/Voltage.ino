// 
// 
// 


void Voltage_setup()
{
	// TODO: make this dynamic
	pinMode(voltEnablePin[0], OUTPUT);
	pinMode(voltEnablePin[1], OUTPUT);
	pinMode(voltEnablePin[2], OUTPUT);
	// 
}

void Voltage_loop()
{

	if (ShouldLoopStart(voltageLoop)) 
	{

		if (voltIsUsed[voltCurrentSensor] == 1) {
			voltRawRead[voltCurrentSensor] = analogRead(voltReadPin[voltCurrentSensor]);    // read the input pin
			voltRawRead[voltCurrentSensor] = analogRead(voltReadPin[voltCurrentSensor]);    // People usually read twice and get the second reading for stability of AnalogRead
			// Conversion to actual voltage: val * AREF / AOUT_AT_AREF * (UPPER_RESISTOR + LOWER_RESISTOR) / LOWER_RESISTOR
			voltage[voltCurrentSensor] = (float)voltRawRead[voltCurrentSensor] * (float)voltARef * ((float)voltUpperR[voltCurrentSensor] + (float)voltLowerR[voltCurrentSensor]) / ((float)voltOutAtARef * (float)voltLowerR[voltCurrentSensor]);
		}
		else {
			voltage[voltCurrentSensor] = 0; 
		}

		// Switch that sensor off
		digitalWrite(voltEnablePin[voltCurrentSensor], LOW);

		// Increase the sensor count so that next time this is run the next sensor is updated
		if (++voltCurrentSensor > voltNumOfSensors) { voltCurrentSensor = 0; }

		// enable the corresponding pin already to make sure the reading is stable when the time to measure comes
		digitalWrite(voltEnablePin[voltCurrentSensor], HIGH);

		// Reset timer so that it won't be run again too eraly
		voltSinceLastPass = 0;
	


		//Serial.print("Raw value: ");
		//Serial.print(rawVoltageRead);             // debug value
		//Serial.print(" Voltage: ");
		//Serial.println(voltage[i]);
		//delay(500);

		// Ack finished
		LoopDone(voltageLoop);
	}
}