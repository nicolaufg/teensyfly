// 
// 
// 



void SdCard_setup()
{
	String currentLine = "";
	String currentSection = "";
	String currentVar = "";
	float currentVarValue = 0;

	// see if the card is present and can be initialized:
	if (!SD.begin(chipSelect)) {
		nextMsg = "Card failed, or not present!";
		Serial.println(nextMsg);
		u8g2_printlnNow(nextMsg);

		return;
	}

	nextMsg = "Card initialized.";
	Serial.println(nextMsg);
	u8g2_printlnNow(nextMsg);

	//////////// READ INI
	ReadIni();
	////////// END READ INI

	String dataString = "";
	dataString = dataString + "Time (ms)";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "Delta T (ms)";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "Gas Pos";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "RPM";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "Glow";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "Volts 0";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "Volts 1";
	dataString = dataString + char(sdColumnSeparator);
	dataString = dataString + "Volts 2";
	dataString = dataString + char(sdColumnSeparator);

	if (writeToFile) {
		
		//time_t StartTime = now() - c_Epoch32OfOriginYear - millis()*1000 ; // Store the current time in time  variable t 
		time_t StartTime = now() - c_Epoch32OfOriginYear ; // Store the current time in time  variable t 


		//dataFileName = StringDateTime(StartTime);
		Serial.println(StringDateTime(StartTime));

		// Create the datalog file to add the header and stuff
		String fileName = MakeFileName(StartTime) + ".txt";

		fileName.toCharArray(dataFileName, 13);
		File dataFile = SD.open(dataFileName, FILE_WRITE);
		Serial.println(dataFileName);

		// if the file is available, write to it:
		if (dataFile)
		{
			// StartTime = month(StartTime) + day(StartTime) + year(StartTime) + hour(StartTime) + minute(StartTime) + second(StartTime);
			dataFile.println(StringDateTime(StartTime));
			dataFile.println(dataString);
			dataFile.close();

			nextMsg = "Data: " + fileName;
			Serial.println(nextMsg);
			u8g2_printlnNow(nextMsg);
		}
		// if the file isn't open, pop up an error:
		else {
			nextMsg = "Error creating Data file!!!";
			Serial.println(nextMsg);
			u8g2_printlnNow(nextMsg);
		}
	}

	if (reportToSerial) {
		Serial.println(dataString);
	}

}

String MakeFileName(const RtcDateTime& dt)
{
	char datestring[9];

	snprintf_P(datestring,
		countof(datestring),
		PSTR("%02u%02u%02u%02u"),
		dt.Month(),
		dt.Day(),
		dt.Hour(),
		dt.Minute());
	return datestring;
}

void SdCard_loop()
{
	
	if (ShouldLoopStart(sdcard) && digitalRead(audioButton) == LOW) {
		
		// make a string for assembling the data to log
		String dataString = "";

		// rough way just for testing
		dataString = dataString + millis();
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + timerLoopPeriod[sdcard];
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + gasPos;
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + rpm;
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + glowState;
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + voltage[0];
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + voltage[1];
		dataString = dataString + char(sdColumnSeparator);
		dataString = dataString + voltage[2];
		dataString = dataString + char(sdColumnSeparator);

		//sdLastPassTime = sdLastPassTime - SD_CYCLE_TIME; // Reset timer

		// open the file. note that only one file can be open at a time,
		// so you have to close this one before opening another.
		if (writeToFile) {
			File dataFile = SD.open(dataFileName, FILE_WRITE);

			if (dataFile) {
				dataFile.println(dataString);
				dataFile.close();
			}
			else {
				Serial.print("Error opening ");
				Serial.println(dataFileName);
			}
		
		}

		if (reportToSerial) {
			Serial.println(dataString);
		}

		// Ack finished
		LoopDone(sdcard);
	}
}



void printErrorMessage(uint8_t e, bool eol = true)
{
	switch (e) {
	case IniFile::errorNoError:
		Serial.print("no error");
		break;
	case IniFile::errorFileNotFound:
		Serial.print("file not found");
		break;
	case IniFile::errorFileNotOpen:
		Serial.print("file not open");
		break;
	case IniFile::errorBufferTooSmall:
		Serial.print("buffer too small");
		break;
	case IniFile::errorSeekError:
		Serial.print("seek error");
		break;
	case IniFile::errorSectionNotFound:
		Serial.print("section not found");
		break;
	case IniFile::errorKeyNotFound:
		Serial.print("key not found");
		break;
	case IniFile::errorEndOfFile:
		Serial.print("end of file");
		break;
	case IniFile::errorUnknownError:
		Serial.print("unknown error");
		break;
	default:
		Serial.print("unknown error value");
		break;
	}
	if (eol)
		Serial.println();
}


int ReadIni() {

	const size_t bufferLen = 200;
	char buffer[bufferLen];

	const char *filename = "/tnsyfly.cfg";



	IniFile ini(filename);
	if (!ini.open()) {
		//Serial.print("Ini file ");
		//Serial.print(filename);
		//Serial.println(" does not exist");
		// Cannot do anything else

		nextMsg = "tnsyfly.cfg NOT found!" ;
		Serial.println(nextMsg);
		u8g2_printlnNow(nextMsg);
		return -1;
	}
	// Serial.println("Ini file exists");


	// Check the file is valid. This can be used to warn if any lines
	// are longer than the buffer.
	if (!ini.validate(buffer, bufferLen)) {
		Serial.print("ini file ");
		Serial.print(ini.getFilename());
		Serial.print(" not valid: ");
		printErrorMessage(ini.getError());
		// Cannot do anything else

		nextMsg = "Invalid File.";
		Serial.println(nextMsg);
		u8g2_printlnNow(nextMsg);

		return -1;
	}

	for (int i = 0; i < iniTotalVarNum; i++) {
		char currentSection[25] ;
		iniSectionNames[i].toCharArray(currentSection, 25);
		char currentVariable[25];
		iniVariableNames[i].toCharArray(currentVariable, 25);

		//if (ini.getValue(currentSection, currentVariable, buffer, bufferLen, i)) { //
		if (ini.getValue(currentSection, currentVariable, buffer, bufferLen, *ptrIniVariables[i])) { //
			nextMsg = "" + iniVariableNames[i] + " set to: " + *ptrIniVariables[i];
			Serial.println(nextMsg);
		}
		else {
			/*Serial.print("Could not read ");
			Serial.print(iniVariableNames[i]);
			Serial.print("from section ");
			Serial.print(iniVariableNames[i]);*/
			nextMsg = "" + iniVariableNames[i] + " failed!";
			Serial.println(nextMsg);
			u8g2_printlnNow(nextMsg);
		}
	}



	

	//// Fetch a value from a section and key
	//if (ini.getValue("Voltage", "voltUpdatePeriod", buffer, bufferLen, voltUpdatePeriod)) {
	//	Serial.print("section 'Voltage' has an entry 'voltUpdatePeriod' with value ");
	//	Serial.println(buffer);
	//}
	//else {
	//	Serial.print("Could not read 'Voltage' from section 'voltUpdatePeriod', error was ");
	//	printErrorMessage(ini.getError());
	//	return -1;
	//}

	nextMsg = "tnsyfly.cfg read";
	Serial.println(nextMsg);
	u8g2_printlnNow(nextMsg);
	
	// and close the ini file
	ini.close();
	
	return 0;
	
	

}


