// AudioRpm.h


///////////////////// RECORD AUDIO STUFF

/////// Common
const bool READ_PEAKS_ONLY = true;
int audioSensorPin = A0;    // That's pin14 in Teensy 3.6select the input pin for the potentiometer
const byte audioButton = 29;
const double MIC_VPP = 0.2; // Microphone peak to peak MOS: 0.2 ELECT: 2
const double MIC_VBIAS = 0.7; // Microphone peak to peak MOS: 0.7 ELECT: 1.25


							  //// Raw reading
int audioSensorValue = 0;  // variable to store the value coming from the sensor
float audioVoltage = 0;
elapsedMicros audioCurrentTime;
const int AUDIO_CYCLE_TIME = 50; //1000Hz

								 //// PeakReader
const int peaksSampleWindow = 4; // Sample window width in mS (50 mS = 20Hz, 4ms=250Hz=15000rpm)
unsigned int peaksSample;

//////////////////// END RECORD AUDIO STUFF
