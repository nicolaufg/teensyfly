// OledDsiplay.h


// Full buffer
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R2, /* reset=*/ U8X8_PIN_NONE);
// One page buffer
//U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R2, /* reset=*/ U8X8_PIN_NONE);
// End of constructor list


uint8_t OLED_REFRESH = 1000;
elapsedMillis u8g2_elapsed = 0;
uint8_t u8g2_duration = 0 ; // duration of this loop.

// Button to set the OLED display to low power
int oledSleepButtonPin = 30;
bool oledIsOff = false;

const uint8_t FONT_HEIGHT = 9;
int currentCursorRow = 0; // Global row position...

// ARTIFICIAL HORIZON
const int HOR_SEP = 14;
const uint8_t HOR_X_CENTER = 18;
const uint8_t HOR_Y_CENTER = 27;
const uint8_t HOR_RAD = 12;
const uint8_t HOR_BOX_W = 36;
const uint8_t HOR_BOX_H = 36;
const uint8_t HOR_TICK_SIZE = 3;

// VOR
const uint8_t VOR_BOX_W = HOR_BOX_W;
const uint8_t VOR_BOX_H = HOR_BOX_H;
const uint8_t VOR_X_CENTER = HOR_X_CENTER + HOR_BOX_W / 2 + VOR_BOX_W / 2 + 1; //128 - VOR_BOX_W / 2
const uint8_t VOR_Y_CENTER = HOR_Y_CENTER;
const uint8_t VOR_RAD = HOR_RAD;
const uint8_t RNWY = 27;
const uint8_t CROSS_LEN = 2;
const uint8_t VOR_TICK_SIZE = 3;


// STATS
const uint8_t ARM_BOX_W = 34;
const uint8_t ARM_BOX_H = (FONT_HEIGHT + 1) * 4; // number of lines
const uint8_t ARM_X_POS = VOR_X_CENTER + VOR_BOX_W / 2 + 1;
const uint8_t ARM_Y_POS = 0 ;


// GLOW
const uint8_t GLO_BOX_W = HOR_BOX_W / 2;
const uint8_t GLO_BOX_H = 7;
const uint8_t GLO_X_START = ARM_X_POS;
const uint8_t GLO_Y_START = ARM_Y_POS + ARM_BOX_H;
const uint8_t GLO_RAD = HOR_RAD;

bool showBottomRpm = true;
bool showBottomGas = true;


// RPM
const uint8_t RPM_X_POS = ARM_X_POS;
const uint8_t RPM_Y_POS = GLO_Y_START + FONT_HEIGHT - 2;

// GPS
const uint8_t GPS_BOX_W = HOR_BOX_W / 2;
const uint8_t GPS_BOX_H = 7;
const uint8_t GPS_X_START = 1;
const uint8_t GPS_Y_START = HOR_Y_CENTER + HOR_BOX_H / 2;
const uint8_t GPS_RAD = HOR_RAD;

