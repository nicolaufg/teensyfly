// Voltage.h
int voltUpdatePeriod = 5000 ; // The routine will not go through until this time has elapsed regardless of the sensor called. Thus each sensor will be called every voltNumOfSensors*voltUpdatePeriod

int voltNumOfSensors = 3; // When the SD card reader is done, this must be used to size the arrays and teh actual data set in the setup loop.

elapsedMillis voltSinceLastPass;

int voltIsUsed[] = { 0, 0,0 }; // Enable/disable that sensor

int voltReadPin[] = { 34, 36, 38 }; // pins where the AnalogRead is to be performed
int voltEnablePin[] = { 33, 35, 37 }; // Pins to open the nMOSFET and read the voltage divider (to save battery!)

int voltRawRead[] = { 0, 0, 0 }; // init of raw reading at pin

float voltage[] = { 0, 0 , 0}; // Value converted to voltage

int voltUpperR[] = {10000, 10000, 10000};
int voltLowerR[] = { 4700, 4700, 4700};
int voltARef = 5;
int voltOutAtARef = 1023;

int voltCurrentSensor = 0;