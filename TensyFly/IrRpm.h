// IrRpm.h

// Pins
int STATUS_LED_PIN = 13; // Status led pin number
int IR_LED_PIN = 26; // IR led pin number
int RPM_SENSOR_PIN = 25; // Whatch out! This is the PIN number, not the interrupt number. I make us of digitalPinToInterrupt(RPM_SENSOR_PIN)

// Timers and boundaries
int RPM_CHECK_INTERVAL = 500;
// TEENSY elapsedMillis sinceLastCount = 0;
unsigned long sinceLastCount = 0;

// Vars and stuff
volatile int rpmPulses; // this may happen fast (ok, not really... max around 350Hz for 20000rpm)
unsigned int rpm, maxRPM, minRPM;
int statusLedStatus = LOW;
int irrpmFlashesPerRev = 4; // of IR HIGH per each revolution. Make it an integer of 60.

int RPM_UPPER_LIMIT = 10000;
int RPM_LOWER_LIMIT = 0;


// Constructors not needed if sources are called xxx.ino
//void IrRpmLoop();
//void IrRpm_setup();
//void RPMCount();
//void ResetMinMaxRPM();

