// MAVLink.h



#include "C:\Users\Nicolau\Documents\Arduino\libraries\mavlink\mavlink.h" // Mavlink interface


int armed = 0;
int vor = 0;
int load = 0;
int volts = 0;

elapsedMillis mavlink_elapsed = 0;
uint8_t MAVLINK_REFRESH = 100;
mavlink_global_position_int_t globalPosition;
mavlink_local_position_ned_t localPosition;
mavlink_gps_raw_int_t gpsRaw;
mavlink_attitude_t attitude;
mavlink_sys_status_t sysStatus;
mavlink_heartbeat_t heartbeat;
mavlink_vfr_hud_t vfrHud;