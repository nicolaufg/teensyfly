// 
// 
// 

void RecordAudio_setup() {
	////////////////////// AUDIO STUFF
	pinMode(audioButton, INPUT_PULLUP);
	/////////////// END AUDIO STUFF
}

void RecordAudio_loop() {

	// read the value from the sensor:
	audioSensorValue = analogRead(audioSensorPin);

	audioVoltage = audioSensorValue * (5.0 / 1023.0) - 2.5;

	Serial.print(micros(), DEC);
	Serial.print(" ");
	Serial.println(audioSensorValue, DEC); // show raw sensor value instead of audioVoltage so that I don't mess up with volt conversion (which is not too clear to me nyway)


}

void ReadPeaks_loop()
{
	unsigned long startMillis = millis();  // Start of sample window
	unsigned int peakToPeak = 0;   // peak-to-peak level

	unsigned int signalMax = 0;
	unsigned int signalMin = 1024;

	unsigned int oldTimeMax = 0;
	unsigned int timeMax = 0;

	// collect data for peaksSampleWindow mS
	while (millis() - startMillis < peaksSampleWindow)
	{
		peaksSample = analogRead(audioSensorPin);
		if (peaksSample < 1024)  // toss out spurious readings
		{
			if (peaksSample > signalMax)
			{
				signalMax = peaksSample;  // save just the max levels
				timeMax = millis();
			}
			else if (peaksSample < signalMin)
			{
				signalMin = peaksSample;  // save just the min levels
			}
		}
	}
	// oldTimeMax = startMillis-timeMax;
	peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
	double peakVolts = (peakToPeak * 5.0) / 1024;  // convert to volts
	Serial.print(timeMax);
	Serial.print(" ");
	Serial.println(peakToPeak); // show raw sensor value instead of peakToPeak so that I don't mess up with volt conversion (which is not too clear to me nyway)

}



