// 
// 
// 

// #include "RTClock.h"


void RTClock_setup() {

	Rtc.Begin();


	printDateTime(compiled);
	Serial.println();
	printDateTime(defaultTime);
	Serial.println();

	if (!Rtc.IsDateTimeValid())
	{
		// Common Cuases:
		//    1) first time you ran and the device wasn't running yet
		//    2) the battery on the device is low or even missing

		nextMsg = "RTC->Default";
		Serial.println(nextMsg);
		u8g2_printlnNow(nextMsg);
		
		Rtc.SetDateTime(defaultTime);
	}

	if (!Rtc.GetIsRunning())
	{
		nextMsg = "RTC->Need Start";
		Serial.println(nextMsg);
		u8g2_printlnNow(nextMsg);
		//Serial.println("RTC was not actively running, starting now");
		Rtc.SetIsRunning(true);
	}

	// never assume the Rtc was last configured by you, so
	// just clear them to your needed state
	Rtc.Enable32kHzPin(false);
	Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);


	// Synch Teensy clock with RTC
	currentTime = Rtc.GetDateTime();
	setTime(currentTime + c_Epoch32OfOriginYear);
	nextMsg = "RTC->Clock Synch";
	Serial.println(nextMsg);
	u8g2_printlnNow(nextMsg);

	printDateTime(now() - c_Epoch32OfOriginYear);
	Serial.println();
}

void RTClock_loop() {

}


#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt)
{
	char datestring[20];

	snprintf_P(datestring,
		countof(datestring),
		PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
		dt.Month(),
		dt.Day(),
		dt.Year(),
		dt.Hour(),
		dt.Minute(),
		dt.Second());
	Serial.print(datestring);
}

String StringDateTime(const RtcDateTime& dt)
{
	char datestring[20];

	snprintf_P(datestring,
		countof(datestring),
		PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
		dt.Month(),
		dt.Day(),
		dt.Year(),
		dt.Hour(),
		dt.Minute(),
		dt.Second());
	return datestring;
}