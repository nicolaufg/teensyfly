// 
// 
// 

//#include "IrRpm.h"


void IrRpm_setup()
{
	//Serial.begin(115200);
	// Rview the pin assignement below. Most of the pins in a Teensy can be interrupted.
	attachInterrupt(digitalPinToInterrupt(RPM_SENSOR_PIN), RPMCount, RISING); // Interrupt when the sensor detects IR light

	rpmPulses = 0;
	rpm = 0;
	ResetMinMaxRPM();

	pinMode(STATUS_LED_PIN, OUTPUT);
	pinMode(IR_LED_PIN, OUTPUT);
	digitalWrite(IR_LED_PIN, HIGH); // light up the IR already

	//sinceLastCount = 0; // millis counter since last time the RPM were counted
}

void IrRpm_loop()
{
	if (ShouldLoopStart(irrpm)) {
		//rpm = 15 * rpmPulses / ((millis() - sinceLastCount) / 1000); // Interval must be in s
		//sinceLastCount = millis();

		rpm = ((60 * 1000/RPM_CHECK_INTERVAL)/irrpmFlashesPerRev) * rpmPulses / ((timerActualLoopPeriod[irrpm]) / 1000); // Interval must be in s

		// Store max and min
		if (rpm > maxRPM) maxRPM = rpm;
		if (rpm < minRPM) minRPM = rpm;

		rpmPulses = 0;

		// Ack finished
		LoopDone(irrpm);

	}
}

void RPMCount() // Invoked by the IR interruption
{
	// what about debouncing???
	rpmPulses++;
	statusLedStatus = !statusLedStatus;
	digitalWrite(STATUS_LED_PIN, statusLedStatus);
}

void ResetMinMaxRPM() {
	maxRPM = 0;
	minRPM = 65300;
}