// GlowDriver.h

int glowUseRPM = 1;
int glowUseGas = 1 - glowUseRPM;

volatile int gasPwmRawValue = 0; // that's in micros. Use volatile to keep it in RAM
volatile int gasPwmRiseTime = 0;

elapsedMillis glow_elapsed = 0;
elapsedMillis glow_button_down = 0;



int glowMode = 0; // 0-off, 1-on, 2-on below X%, 3-pulsed below X%
int glowCycleTime = 500; // millis
int gasThreshold = 1200;
int gasPwmMin = 900;
int gasPwmMax = 1678;
int gasPos = 1200; // Gas position corrected by 'inverseGas'
boolean glowIsPulsed = false;
boolean glowIsEnabled = false;
boolean glowHasThreshold = false;
int gasServoIsInverted = 1;

int rpmThreshold = 2500; // in case the glow is driven by RPM

// pins
int gasPwmPin = 38; // SERVO IS AT 5V!!!!!!!!!!!!!!!!!!
int glowPin = 27;
int glowButton = 28;
int debounce = 200; // max 255
int shortWait = 4000;
int longWait = 10000;
int GAS_MIN_SAFETY = 0; // Leave this to zero and make sure the calibration is done with a bit of trim down.


int glowState = LOW;             // Output to glowPin

