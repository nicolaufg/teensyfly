/*
 Name:		TensyFly.ino
 Created:	7/29/2017 5:20:04 PM
 Author:	Nico
*/


#include "MPXTelemetry.h"
#include <TimeLib.h>
#include <Time.h>
#include <RtcUtility.h>
#include <RtcTemperature.h>
#include <RtcDS3231.h>
#include <RtcDS1307.h>
#include <RtcDateTime.h>
#include <IPAddress.h>
//#include <File.h>
#include <Ethernet.h>
//#include <arduino_compat.h>
#include <IniFile.h>
#include <SD.h>
#include <SPI.h>

#include <U8g2lib.h> // OLED - Compiler complaints if these two include are in OledDisplay.h
#include <Wire.h> // as it goes through I2C
//


#include "Timers.h"

#include "Voltage.h"
#include "OledDisplay.h"
#include "AudioRpm.h"
#include "MAVLink.h"
//#include "UAVTalkComms.h"
#include "GlowDriver.h"
#include "IrRpm.h"
#include "RTClock.h"
#include "SdCard.h"


elapsedMillis myWait = 0; // General elapsedMillis to be used anywhere. Keep it local to prevent interference.


void setup()
{
	Serial1.begin(57600); // to Flight Controller
	Serial.begin(115200); // to USB

	u8g2_setup();
	// FULL BUFFER
	u8g2.clearBuffer();
	u8g2_prepare();
	
	RTClock_setup();
	IrRpm_setup();
	RecordAudio_setup();
	GlowDriver_setup();
	MAVLink_setup();
	Voltage_setup();
	SdCard_setup();

	MPXTelemetry_Setup();


	// Whatever written in the buffer needs to be printed.
	u8g2.sendBuffer();

	// PAGE BUFFER
	//u8g2_prepare();
	//u8g2.firstPage();
	//do {
	//	u8g2.setCursor(0, 0);
	//	u8g2.print(sdConfigResultString);
	//} while (u8g2.nextPage());
	delay(10000);
}


void loop() {

	// Voltage readings. Only one sensor every loop will be updated due to AnalogRead issues
	Voltage_loop(); 

	// Receive telemetry
	MAVLinkReceive();

	// Handle the glow
	GlowDriver_loop();

	// Read RPM
	IrRpm_loop();

	// SD Card (looger)
	SdCard_loop();

	// OLED display
	u8g2_loop();

	// Talk to the MULTIPLEX Arduino
	MPXTelemetry_Loop();


	////Serial.println(audioCurrentTime);
	//if (digitalRead(audioButton) == LOW )
	//{
	//	if (READ_PEAKS_ONLY) {
	//		ReadPeaks_loop();
	//	}
	//	else {
	//		if (audioCurrentTime >= AUDIO_CYCLE_TIME) {
	//			audioCurrentTime = audioCurrentTime - AUDIO_CYCLE_TIME;
	//			RecordAudio_loop();
	//		}
	//	}
	//	
	//}
	//else // do OLED only if NOT recoding
	//{
	//	u8g2_loop();
	//}
}





