// RTClock.h


//#if defined(ESP8266)
//#include <pgmspace.h>
//#else
//#include <avr/pgmspace.h>
//#endif





#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);

RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
RtcDateTime defaultTime = RtcDateTime("Jan 01 2017", "10:00:00");
RtcDateTime currentTime;