// 
// 
// 

//#include "Timers.h"



bool ShouldLoopStart(loops loopName) {
	timerActualLoopPeriod[loopName] = timerLoopPeriod[loopName];
	if (timerLoopPeriod[loopName] >= loopUpdatePeriod[loopName] ) {
		timerLoopPeriod[loopName] = timerLoopPeriod[loopName] - (int)loopUpdatePeriod[loopName]; // Reset inter loop timer
		timerLoopDuration[loopName] = millis(); // Start intra loop timer
		return true;
	}
	else return false;
}

void LoopDone(loops loopName) {
	timerLoopDuration[loopName] = millis() - timerLoopDuration[loopName];

	if (loopName == mavlink  && mavlinkMsgsBars) {
		if (thisLoopRecMessages == 0) return;

		for (int i = 0; i <= thisLoopRecMessages; i++) {
			Serial.print(">");
		}
		Serial.println(" ");
	}






	if (printLoopTimes[loopName]) {
		Serial.print(millis());
		Serial.print(" ");
		Serial.print(loopNames[loopName]);
		Serial.print("->");

		if (loopName == mavlink) {
			Serial.print(" MsgRec: ");
			Serial.print(thisLoopRecMessages);
		} else { 
			Serial.print(" "); 
			Serial.print(" ");
		}

		Serial.print(" Period: ");
		Serial.print(timerActualLoopPeriod[loopName]);
		Serial.print(" Duration: ");
		Serial.println(timerLoopDuration[loopName]);

	
	}
}

