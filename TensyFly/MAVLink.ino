// 
// 
// 

// Mavlink info:
//# http://diydrones.com/forum/topics/beginning-tests-using-mavlink?id=705844%3ATopic%3A1142311&page=2#comments

void MAVLink_setup() {

}

void MAVLinkReceive() {

	if (ShouldLoopStart(mavlink)) // the period for this will be usually 0... actual condition is the Serial1.available below. But we want the inner loop timer
	{

	mavlink_message_t msg;
	mavlink_status_t status;
	thisLoopRecMessages = 0;

	// COMMUNICATION THROUGH EXTERNAL UART PORT (Serial1 Serial1)

	while (Serial1.available() > 0)
	{
		thisLoopRecMessages = thisLoopRecMessages + 1;
		uint8_t c = Serial1.read();
		// Try to get a new message
		if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status)) {
			// Handle message
			
			
			switch (msg.msgid)
			{
			case MAVLINK_MSG_ID_HEARTBEAT: //HEARTBEAT LISTEN CASE
			{
				mavlink_msg_heartbeat_decode(&msg, &heartbeat);

				if (heartbeat.base_mode >> 7)
					armed = 1;
				else armed = 0;
			}
			break;

			case MAVLINK_MSG_ID_ATTITUDE:
			{
				mavlink_msg_attitude_decode(&msg, &attitude);
			}
			break;

			case MAVLINK_MSG_ID_SYS_STATUS:
			{
				//load = mavlink_msg_sys_status_get_load(&msg) / 10;
				//volts = mavlink_msg_sys_status_get_voltage_battery(&msg);
				mavlink_msg_sys_status_decode(&msg, &sysStatus);
			}
			break;

			case 	MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
			{
				mavlink_msg_global_position_int_decode(&msg, &globalPosition);
			}
			break;

			case 	MAVLINK_MSG_ID_LOCAL_POSITION_NED:
			{
				mavlink_msg_local_position_ned_decode(&msg, &localPosition);
			}
			break;

			case 	MAVLINK_MSG_ID_GPS_RAW_INT:
			{
				mavlink_msg_gps_raw_int_decode(&msg, &gpsRaw);
				//mavlink_msg_local_position_ned_decode(&msg, &localPosition);
			}
			break;

			case 	MAVLINK_MSG_ID_VFR_HUD:
			{
				mavlink_msg_vfr_hud_decode(&msg, &vfrHud);
				/*typedef struct __mavlink_vfr_hud_t
				{
					float airspeed; ///< Current airspeed in m/s
					float groundspeed; ///< Current ground speed in m/s
					float alt; ///< Current altitude (MSL), in meters
					float climb; ///< Current climb rate in meters/second
					int16_t heading; ///< Current heading in degrees, in compass units (0..360, 0=north)
					uint16_t throttle; ///< Current throttle setting in integer percent, 0 to 100
				} mavlink_vfr_hud_t;*/
			}
			break;


			/*case MAVLINK_MSG_ID_COMMAND_LONG:
			{
			Serial.print(msg.sysid);
			Serial.print(msg.compid);
			Serial.print(mavlink_msg_heartbeat_get_type(&msg));
			Serial.println(msg.sysid);
			}
			break;*/

			default:
				//Do nothing
				break;
			}

		}

		// And get the next one
	}

	// Ack finished
	LoopDone(mavlink);
	}
}
